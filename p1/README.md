# LIS4331: Advanced Mobile Applications Development

## Alexander Barlow

### Project #1 Requirements:

*Sub-Heading:*

1. Include splash screen image, app title, intro text.
2. Includes artist's images and media.
3. Images and buttons must be vertically and horizontally alligned.
4. Must add background color(s) or theme
5. Create and displaylauncher icon image
6. Chapter Questions.
7. BitBucket repo link.

#### README.md file should include the following items:

* Screenshot of running application's **splash screen**.
* Screenshot of running application's **follow-up screen** (with images and buttons).
* Screenshot of running application's **play and pause** user intrfaces (with images and 
buttons).


#### Assignment Screenshots:

*Screenshots Splash Screen, Follow-up Screen, Play and Pause User Interface*:

| ![Splash Screen](img/splash.png) | ![Follow-up Screen](img/followup.png) |

| ![Pause Screen with all buttons not visable](img/pause.png) | ![Play Screen with buttons visable](img/play.png) |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial 
Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket 
Station Locations")

