# LIS4331: Advanced Mobile Applications Development

## Alexander Barlow

### Project #2 Requirements:

*Sub-Heading:*

1. Include splash screen.
2. Insert at least five sample tasks.
3. Test database class.
4. Must add background color(s) or theme
5. Create and displaylauncher icon image
6. Chapter Questions.
7. BitBucket repo link.

#### README.md file should include the following items:

* Screenshot of running applications **splash screen**.
* Screenshot of running application's Task List. *


#### Assignment Screenshots:

*Screenshots Task List and Splash Screen*:

| ![Task List Screenshot](img/task.png) | ![Splash Screen Screenshot](img/splash.png) |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial 
Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket 
Station Locations")

