> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4331: Advanced Mobile Applications Development

## Alexander Barlow

### Assignment #1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and BitBucket
2. Development Installations
3. Chapter Questions
4. BitBucket repo links

#### README.md file should include the following items:

* Screenshot of **JDK java Hello**
* Screenshot of **Android Studio My First App**
* Screenshot of  **Android Studio Contacts App**
* git commands with short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates a new empty Git repository or initilizes an existing one 
2. git status - displays the state of the working directory and staging area 
3. git add - adds a change from working directory to staging area
4. git commit - saves changes to local respository
5. git push - uploads local repository to remote repository
6. git pull - pulls files and changes from remote repository to local repository
7. git init --help - provides a synopsis of the most commonly used commands

#### Assignment Screenshots:

*Screenshots of AMPPS running http://localhost*:

| ![AMPPS Installation Screenshot](img/ampps1.png) | ![AMPPS Installation Screenshot](img/ampps2.png) |

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial 
Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket 
Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
