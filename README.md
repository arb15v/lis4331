> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 4331: Advanced Mobile Applications Development

## Alexander Barlow

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio
    - Provide screenshots of work
    - Create BitBucket repo
    - Complete BitBucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Go through step-by-step tutorial of Ch. 3 
    - Create dropdown menus, background color, and launcher icon and display
    - Provide screenshots of work

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Go through step-by-step tutorial of Ch. 4
    - Create Radiobuttons, color, launcher icon, and toast notification
    - Provide screenshots of work

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Go through step-by-step tutorial of Ch. 11
    - Create splash screen, SharedPreferences, launcher icon, and splash screen
    - Provide screenshots of work

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Follow videos and code provided
    - Create splash screen, launcher icon, RSSfeed files and xml files
    - Provide screenshots of work

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Go through step-by-step tutorial of Ch. 6
    - Create splash screen, Media Player, horizontal and vertical allignment
    - Provide screenshots of work

6. [P2 README.md](p2/README.md "My P2 README.md file")
    - Follow videos and images provided
    - Create splash screen
    - Generate five sample tasks and test database class
    - Provide screenshots of work

