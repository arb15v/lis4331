

# LIS4331: Advanced Mobile Applications Development

## Alexander Barlow

### Assignment #2 Requirements:

*Sub-Heading:*

1. Dropdown menu for total number of guests from 1-10.
2. Dropdown menu for tip percentage from 0% to 25%.
3. Must add background color(s) or theme.
4. Must create and display launcher icon image.
5. Chapter Questions.
6. BitBucket repo link.

#### README.md file should include the following items:

* Screenshot of running applications **unpopulated** user interface.
* Screenshot of running applications **populated** user interface.


#### Assignment Screenshots:

*Screenshots of Populated and Unpopulated user interface*:

| ![Populated User Interface Screenshot](img/pop.png) | ![Unpopulated User Interface Screenshot](img/unpop.png) 
|


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial 
Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket 
Station Locations")

