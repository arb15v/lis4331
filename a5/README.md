

# LIS4331: Advanced Mobile Applications Development

## Alexander Barlow

### Assignment #5 Requirements:

*Sub-Heading:*

1. Include splash screen image, app title, intro text.
2. Must use your own RSS feed.
3. Must add background color(s) or theme.
4. Create and displaylauncher icon image.
5. Chapter Questions.
6. BitBucket repo link.

#### README.md file should include the following items:

* Screenshot of running applications **splash screen (listof articles - artivity_items.xml)**.
* Screenshot of running applications **individual article** (**activity_item.xml**).
* Screenshots of running applications **default browser** (article link).


#### Assignment Screenshots:

*Screenshots of Splash Screen, Individual Article, and Default Browser*:

| ![Splash Screen Screenshot](img/splash.png) | ![Individual Article Screenshot](img/art.png) | ![Default Browser Screenshot](img/browser.png) |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial 
Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket 
Station Locations")

