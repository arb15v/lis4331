import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.text.DecimalFormat;

 public class a2b2c2 implements ActionListener {
 
   public static void main(String[] args) {
      a2b2c2 gui = new a2b2c2();
   }//end public

   private JFrame frame;
   private JTextField legA;
   private JTextField legB;
   private JLabel legC;
   private JButton computeButton;

   public a2b2c2() {

      legA = new JTextField(5);
      legB = new JTextField(5);
      legC = new JLabel("Compute Distance Leg C");
      computeButton = new JButton("Compute");

      computeButton.addActionListener(this);

      JPanel north = new JPanel(new GridLayout(2, 2));
      north.add(new JLabel("Leg A: "));
      north.add(legA);
      north.add(new JLabel("Leg B: "));
      north.add(legB);


      frame = new JFrame("Pythagorean Theorem");
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setLayout(new BorderLayout());
      frame.add(north, BorderLayout.NORTH);
      frame.add(legC, BorderLayout.CENTER);
      frame.add(computeButton, BorderLayout.SOUTH);
      frame.pack();
      frame.setVisible(true);
   }//end a2b2c2

 
   public void actionPerformed(ActionEvent event) {

      String legAText = legA.getText();
      double lega = Double.parseDouble(legAText);
      String legBText = legB.getText();
      double legb = Double.parseDouble(legBText);

 
      DecimalFormat yes = new DecimalFormat("#0.00");
      double legc = (lega * lega) + (legb * legb);
      legC.setText("Leg C: " + yes.format(Math.sqrt(legc)));
   }//end ActionEvent

}//end classs