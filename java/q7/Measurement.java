import java.util.Scanner;


public class Measurement
{
   
   public static void main(String[] args)
    {
       class Constants
       {
        //declare constants
        static final double INCHES_TO_CENTIMETER = 2.54;
        static final double INCHES_TO_METER = 0.0254;
        static final double INCHES_TO_FOOT = 12;
        static final double INCHES_TO_YARD = 36;
        static final double FEET_TO_MILE = 5280;
       }//end of Constants

       //declare vari
       int inches = 0;
       double centimeter = 0;
       double meter = 0;
       double feet = 0;
       double yard = 0;
       double mile = 0;
       boolean valid;

       // scanner
       Scanner input = new Scanner(System.in);

       //notes
       System.out.println("Program converts inches to centimeters, meters, feet, rads, and miles.");
       System.out.println("***Notes***:");
       System.out.println("1) Use integer for inches (must validate integer input)");
       System.out.println("2) Use print(f) function to print (format values per below input)");
       System.out.println("3) Create Java \"constant\" for the following values:");
       System.out.println("        INCHES_TO_CENTIMETER");
       System.out.println("        INCHES_TO_METER");
       System.out.println("        INCHES_TO_FOOT");
       System.out.println("        INCHES_TO_YARD");
       System.out.println("        FEET_TO_MILE");
       System.out.print("\n\n");

       //do while loop

       do{

        System.out.print("Please enter number of inches: ");

        if (input.hasNextInt()) {
            
            inches = input.nextInt();
            valid = true;

        }else{
            
            System.out.print("Incorrect Entry. Try again. ");
            valid = false;
            input.next();
            }

        }while (!(valid));

        //calculations

        centimeter = inches * Constants.INCHES_TO_CENTIMETER;
        meter = inches * Constants.INCHES_TO_METER;
        feet = inches / Constants.INCHES_TO_FOOT;
        yard = inches / Constants.INCHES_TO_YARD;
        mile = (inches / Constants.INCHES_TO_FOOT) / Constants.FEET_TO_MILE;

        //printed to screen
        System.out.println("" + inches + " inch(es) equals...\n");
        System.out.printf("Centimeter(s): %.6f ", centimeter);
        System.out.print("\n");
        System.out.printf("Meter(s): %.6f ", meter);
        System.out.print("\n");
        System.out.printf("Feet: %.6f ", feet);
        System.out.print("\n");
        System.out.printf("Yard(s): %.6f ", yard);
        System.out.print("\n");
        System.out.printf("Mile(s): %.8f ", mile);
        System.out.print("\n");


        

   }//end if main
}//end of Measurement