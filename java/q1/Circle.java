import java.util.Scanner;

public class Circle
{
   public static void main(String[] args)
   {
    double input = 0;
    double diameter;
    double circumference;
    double area;
    boolean valid;
      
      Scanner sc = new Scanner(System.in);

       System.out.println("Non-OOP program calculates diameter, circumference, and circle area.");
       System.out.println("Must use Java's built-in PI constant, printf(), and formatted to two decimal places." );
       System.out.println("Must *only* permit numeric entry.");
       System.out.print( "\n\n" );

       do{

        System.out.print("Enter circle radius: " );

        if (sc.hasNextDouble()) {
            
            input = sc.nextDouble();
            valid = true;

        }else{
            
            System.out.print("Incorrect Entry. Try again. ");
            valid = false;
            sc.next();
            }

        }while (!(valid));

            diameter = input * 2;
            circumference = input * 2 * Math.PI;
            area = input * input * Math.PI;

            System.out.print( "\n\n" );
            System.out.printf("Circle diameter: %.2f feet", diameter );
            System.out.print( "\n\n" );
            System.out.printf("Circle circumference: %.2f feet", circumference );
            System.out.print( "\n\n" );
            System.out.printf("Circle area: %.2f feet", area );
            System.out.print( "\n\n" );

        }//end main
    }//end Circle