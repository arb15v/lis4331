import java.util.Scanner;

public class MultipleNumber
{
   public static void main(String[] args)
   {
    int Num1;
    int Num2;
      
      Scanner sc = new Scanner(System.in);

       System.out.println("Program determines if first  number is multiple of second, prints results.");
       System.out.println("Example: 2, 4, 6, 8 are multiples of 2." );
       System.out.println("1) Use intergers. 2) Use printf() funtion to print.");
       System.out.println("Must *only* permit interger entry.");
       System.out.print( "\n\n" );


        System.out.print("Num1: " );

        while  (!sc.hasNextInt()) {
            
            System.out.println("Not a valid integer!\n");
            sc.next();
            System.out.print("Please try again. Enter Num1: ");
        }
            Num1 = sc.nextInt();

            System.out.print("Num2: ");
            
        while (!sc.hasNextInt()) {
            System.out.println("Not a valid integer!\n");
            sc.next();
            System.out.print("Please try again. Enter Num2: ");

        }

            Num2 = sc.nextInt();


            if(Num1 % Num2 == 0){

                System.out.println(Num1 + " is a multiple of " + Num2);
                System.out.printf("The product of " + (Num1/Num2) + " and " + Num2 + " is " + Num1 );

            }else{

                System.out.println(Num1 + "is not a multiple of" + Num2);
            }

        }//end main
    }//end Circle