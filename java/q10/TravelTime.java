import java.util.Scanner;
import java.lang.System;
import java.text.DecimalFormat;

public class TravelTime
{
    public static void main(String[] args)
    {
        //variables
        double miles = 0;
        double MPH = 0;
        boolean valid;
        boolean valid2;
        String YorN = "";

        //scanner
        Scanner input = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);
        Scanner input3 = new Scanner(System.in);

        do{
            //beginning code
            System.out.print("\n");
            System.out.println("Program calculates approximate travel time.");

            //do while loop x2
            do{
                System.out.print("Please enter miles from location: ");

                if (input.hasNextDouble()) 
                {
                    miles = input.nextDouble();
                    if (miles <= 3000 && miles > 0){
                    
                        valid = true;

                    }else{
                        
                        System.out.print("\n");
                        System.out.println("Miles must be greater than 0, and no more than 3000.");
                        System.out.print("\n");
                        valid = false;

                    }//end else

                }else{
                    
                    System.out.print("\n");
                    System.out.println("Invalid double--Miles must be a number. ");
                    System.out.print("\n");
                    valid = false;
                    input.next();

                }//end else

            }while (!(valid));

            do{
                System.out.print("Please enter MPH: ");

                if (input2.hasNextDouble()) 
                {
                    MPH = input2.nextDouble();
                    if (MPH <= 100 && miles > 0){
                    
                        valid2 = true;

                    }else{

                        System.out.print("\n");
                        System.out.println("MPH must be greater than 0, and no more than 100.");
                        System.out.print("\n");
                        valid2 = false;

                    }//end else

                }else{
                    
                    System.out.print("\n");
                    System.out.println("Invalid double-- MPH must be a number. ");
                    System.out.print("\n");
                    valid2 = false;
                    input2.next();

                }//end else

            }while (!(valid2));

            //calculations

            double MPM = MPH / 60;
            double time = miles / MPM;

            int hours = (int) time / 60;
            int minutes = (int) time % 60;;

            DecimalFormat df = new DecimalFormat("00");

            //print to screen
        
            System.out.print("\n");
            System.out.println("Estimated travel time: " + hours + " hour(s) and " + df.format(minutes) + " minute(s).");

            System.out.print("\n");
            System.out.print("Continue? (y/n): ");

            YorN = input3.nextLine();

        }while (YorN.equals("y") );
    }//end main
}//end Travel Time