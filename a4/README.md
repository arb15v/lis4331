

# LIS4331: Advanced Mobile Applications Development

## Alexander Barlow

### Assignment #3 Requirements:

*Sub-Heading:*

1. Include splash screen image, app title, intro text.
2. Include appropriate images.
3. Must use persistant data: SharedPreferences.
4. Widgets and Images must be vertically and horizontally alligned.
5. Must add background color(s) or theme.
6. Create and displaylauncher icon image.
7. Chapter Questions.
8. BitBucket repo link.

#### README.md file should include the following items:

* Screenshot of running applications **splash screen**.
* Screenshot of running applications **invalid sreen** (with appropriate image).
* Screenshots of running applications **valid screen** (with appropriate image).


#### Assignment Screenshots:

*Screenshots of Splash Screen, Invalid Screen, and Valid Screen*:

| ![Splash Screen Screenshot](img/splash.png) | ![Default Screen Screenshot](img/default.png) | ![Invalid Screen Screenshot](img/invalid.png) |

| ![Valid Screen 10 Screenshot](img/ten.png) | ![Valid Screen 20 Screenshot](img/twenty.png) | ![Valid Screen 30 Screenshot](img/thirty.png) |


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial 
Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket 
Station Locations")

