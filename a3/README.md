

# LIS4331: Advanced Mobile Applications Development

## Alexander Barlow

### Assignment #3 Requirements:

*Sub-Heading:*

1. Field to enter U.S. dollar amount: 1–100,000.
2. Must include toast notification if user enters out-of-range values.
3. Radio buttons to convert from U.S. to Euros, Pesos and Canadian currency (must 
be vertically and horizontally aligned)
4. Must include correct sign for euros, pesos, and Canadian dollars
5. Must add background color(s) or theme
6. Create and displaylauncher icon image
7. Chapter Questions.
8. BitBucket repo link.

#### README.md file should include the following items:

* Screenshot of running applications **unpopulated** user interface.
* Screenshot of running applications **toast notification**.
* Screenshot of running applications **converted currency** user interface.


#### Assignment Screenshots:

*Screenshots Unpopulated User Interface, Toast Notification, and Converted Currency*:

| ![Unpopulated User Interface Screenshot](img/unpop.png) | ![Toast Notification Screenshot](img/toast.png) |
![Converted Currency Screenshot](img/3.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial 
Link](https://bitbucket.org/arb15v/bitbucketstationlocations/ "Bitbucket 
Station Locations")

